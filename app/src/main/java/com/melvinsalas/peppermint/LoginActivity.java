package com.melvinsalas.peppermint;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.melvinsalas.utils.TextInputLayoutUtil;

import static com.melvinsalas.utils.TextInputLayoutUtil.*;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        firebaseAuth = FirebaseAuth.getInstance();
        addListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(firebaseAuth.getCurrentUser() != null) {
            login();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signInButton:
                signIn();
                break;
            case R.id.signUpButton:
                signUp();
                break;
        }
    }

    private void addListener() {
        getSignIn().setOnClickListener(this);
        getSignUp().setOnClickListener(this);
    }

    private void signUp() {
        Intent signUp = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(signUp);
    }

    private void signIn() {
        if(isFormValid()) {
            String email = TextInputLayoutUtil.getString(getEmail());
            String password = TextInputLayoutUtil.getString(getPassword());

            if(email != null && password != null) {
                firebaseAuth
                        .signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(getOnCompleteListener());
            }
        }
    }

    private void login() {
        Intent main = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(main);
        finish();
    }

    private OnCompleteListener<AuthResult> getOnCompleteListener() {
        return new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    login();
                } else {
                    Toast.makeText(
                            LoginActivity.this,
                            "Authentication failed.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    private boolean isFormValid() {
        return validateEmail(getEmail()) && validatePassword(getPassword());
    }

    // region Get & Set

    private TextInputLayout getEmail() {
        return findViewById(R.id.emailInput);
    }

    private TextInputLayout getPassword() {
        return findViewById(R.id.passwordInput);
    }

    private Button getSignIn() {
        return findViewById(R.id.signInButton);
    }

    private TextView getSignUp() {
        return findViewById(R.id.signUpButton);
    }

    // endregion
}
