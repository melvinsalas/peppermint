package com.melvinsalas.peppermint;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.melvinsalas.peppermint.database.CategoryManager;
import com.melvinsalas.peppermint.models.CategoryModel;
import com.melvinsalas.utils.TextInputLayoutUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddCategoryActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.nameInput) public TextInputLayout nameInput;

    private CategoryModel category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        ButterKnife.bind(this);

        addListener();

        if(getIntent().hasExtra("category")){
            category = getIntent().getParcelableExtra("category");
            nameInput.getEditText().setText(category.name);
        } else {
            category = null;
        }
    }

    private void addListener() {
        getCreateButton().setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.createCategory:
                if (category == null) {
                    createCategory();
                } else {
                    updateCategory();
                }
                break;
        }
    }

    private void updateCategory() {
        CategoryManager.update(
                category.getId(),
                TextInputLayoutUtil.getString(nameInput),
                getResources().getColor(R.color.colorPrimary),
                getOnCompleteListener());
    }

    private OnCompleteListener<Void> getOnCompleteListener() {
        return task -> {
            if (task.isSuccessful()) {
                finish();
            } else {
                Toast.makeText(AddCategoryActivity.this, "Error.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void createCategory() {
        CategoryManager.create(
                TextInputLayoutUtil.getString(nameInput),
                getResources().getColor(R.color.colorPrimary),
                getOnCompleteListener());
    }

    // region Get & Set

    private Button getCreateButton() {
        return  findViewById(R.id.createCategory);
    }

    // endregion
}
