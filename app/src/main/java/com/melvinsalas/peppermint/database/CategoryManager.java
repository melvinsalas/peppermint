package com.melvinsalas.peppermint.database;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ValueEventListener;
import com.melvinsalas.peppermint.models.CategoryModel;

public class CategoryManager {

    private static String COLLECTION = "categories";

    public static void create(String name, int color, OnCompleteListener<Void> onCompleteListener) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        CategoryModel model = new CategoryModel (uid, name, color);
        String key = Database.getReference().push().getKey();
        Database.getReference()
                .child(COLLECTION).child(key).setValue(model)
                .addOnCompleteListener(onCompleteListener);
    }

    public static void update(String id, String name, int color, OnCompleteListener<Void> onCompleteListener) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        CategoryModel model = new CategoryModel (uid, name, color);
        Database.getReference()
                .child(COLLECTION).child(id).setValue(model)
                .addOnCompleteListener(onCompleteListener);
    }

    /**
     * Get all wallets of current user
     *
     * @param listener The value event listener implementation
     */
    public static void getAll(ValueEventListener listener) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null) {
            Database.getReference()
                    .child(COLLECTION).orderByChild("owner").equalTo(user.getUid())
                    .addValueEventListener(listener);
        }
    }
}
