package com.melvinsalas.peppermint.database;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ValueEventListener;
import com.melvinsalas.peppermint.models.WalletModel;

public class WalletManager {

    private static String COLLECTION = "wallets";

    public static void create(String name, double value, int color, OnCompleteListener<Void> onCompleteListener) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        WalletModel model = new WalletModel(uid, name, value, color);
        String key = Database.getReference().push().getKey();
        Database.getReference()
                .child(COLLECTION).child(key).setValue(model)
                .addOnCompleteListener(onCompleteListener);
    }

    public static void update(WalletModel wallet, String name, double value, int color, OnCompleteListener<Void> onCompleteListener) {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        WalletModel model = new WalletModel(uid, name, value, color);
        Database.getReference()
                .child(COLLECTION).child(wallet.getId()).setValue(model)
                .addOnCompleteListener(onCompleteListener);
    }

    /**
     * Get all wallets of current user
     *
     * @param listener The value event listener implementation
     */
    public static void getAll(ValueEventListener listener) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user != null) {
            Database.getReference()
                    .child(COLLECTION).orderByChild("owner").equalTo(user.getUid())
                    .addValueEventListener(listener);
        }
    }
}
