package com.melvinsalas.peppermint.database;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Database {

    private static DatabaseReference databaseReference;
    private static FirebaseDatabase database;

    public static DatabaseReference getReference() {
        if(database == null) {
            database = FirebaseDatabase.getInstance();
        }
        if(databaseReference == null) {
            databaseReference = database.getReference();
        }
        return databaseReference;
    }
}
