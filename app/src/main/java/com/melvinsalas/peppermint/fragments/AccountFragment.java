package com.melvinsalas.peppermint.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.melvinsalas.peppermint.LoginActivity;
import com.melvinsalas.peppermint.R;

public class AccountFragment extends Fragment implements View.OnClickListener {

    private View view;

    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_account, container, false);
        addListener();
        populateAccount();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logOutButton:
                logOut();
                break;
        }
    }

    private void populateAccount() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        getUserText().setText(currentUser.getEmail());
    }

    private void logOut() {
        FirebaseAuth.getInstance().signOut();
        Intent main = new Intent(getActivity(), LoginActivity.class);
        startActivity(main);
        getActivity().finish();
    }

    private void addListener() {
        getLogOutButton().setOnClickListener(this);
    }

    // region Get & Set

    private Button getLogOutButton() {
        return view.findViewById(R.id.logOutButton);
    }

    private TextView getUserText() {
        return view.findViewById(R.id.userText);
    }

    // endregion
}
