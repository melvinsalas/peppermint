package com.melvinsalas.peppermint.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.kennyc.bottomsheet.BottomSheet;
import com.kennyc.bottomsheet.BottomSheetListener;
import com.melvinsalas.peppermint.AddCategoryActivity;
import com.melvinsalas.peppermint.R;
import com.melvinsalas.peppermint.adapters.CategoriesAdapter;
import com.melvinsalas.peppermint.database.Database;
import com.melvinsalas.peppermint.models.CategoryModel;

import java.util.ArrayList;

public class CategoriesFragment extends Fragment implements View.OnClickListener, BottomSheetListener {

    private View view;
    private ArrayList<CategoryModel> categories;
    private CategoryModel selected;

    public CategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_categories, container, false);
        addListener();
        categories = new ArrayList<>();
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addFab:
                addCategory();
                break;
        }
    }

    private void addCategory() {
        Intent main = new Intent(getActivity(), AddCategoryActivity.class);
        startActivity(main);
    }


    private void addListener() {
        getAddFab().setOnClickListener(this);
        addChildEventListener();
        getCategories().setOnItemLongClickListener((parent, view, position, id) -> {
            selected = categories.get(position);
            openMenu();
            return false;
        });
    }

    private void openMenu() {
        new BottomSheet.Builder(getActivity())
                .setSheet(R.menu.item)
                .setTitle("Title")
                .setListener(this)
                .show();
    }

    private void addChildEventListener() {
        ChildEventListener childListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                CategoryModel category = dataSnapshot.getValue(CategoryModel.class);
                category.setId(dataSnapshot.getKey());
                categoryAdded(category);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                CategoryModel wallet = dataSnapshot.getValue(CategoryModel.class);
                wallet.setId(dataSnapshot.getKey());
                categoryChanged(wallet);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                CategoryModel wallet = dataSnapshot.getValue(CategoryModel.class);
                wallet.setId(dataSnapshot.getKey());
                categoryRemoved(wallet);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Database.getReference()
                .child("categories").orderByChild("owner").equalTo(uid)
                .addChildEventListener(childListener);
    }

    private void categoryRemoved(CategoryModel category) {
        categories.removeIf(c -> c.getId().equals(category.getId()));
        if(getActivity() != null) {
            CategoriesAdapter adapter = new CategoriesAdapter(getActivity(), categories);
            getCategories().setAdapter(adapter);
        }
    }

    private void categoryChanged(CategoryModel wallet) {
        for(int i = 0; i < categories.size(); i++) {
            if(categories.get(i).getId().equals(wallet.getId())) {
                categories.set(i, wallet);
            }
        }
        if(getActivity() != null) {
            CategoriesAdapter adapter = new CategoriesAdapter(getActivity(), categories);
            getCategories().setAdapter(adapter);
        }
    }

    private void categoryAdded(CategoryModel category) {
        categories.add(category);
        if(getActivity() != null) {
            CategoriesAdapter adapter = new CategoriesAdapter(getActivity(), categories);
            getCategories().setAdapter(adapter);
        }
    }

    // region Get & Set

    private FloatingActionButton getAddFab() {
        return view.findViewById(R.id.addFab);
    }

    private ListView getCategories() {
        return view.findViewById(R.id.categoriesList);
    }

    @Override
    public void onSheetShown(@NonNull BottomSheet bottomSheet, @Nullable Object o) {

    }

    @Override
    public void onSheetItemSelected(@NonNull BottomSheet bottomSheet, MenuItem menuItem, @Nullable Object o) {
        switch (menuItem.getItemId()) {
            case R.id.edit:
                editCategory();
                break;
            case R.id.delete:
                removeCategory(selected);
                break;
        }
    }

    private void removeCategory(CategoryModel selected) {
        Database.getReference()
                .child("categories").child(selected.getId()).removeValue();
    }

    private void editCategory() {
        Intent main = new Intent(getActivity(), AddCategoryActivity.class);
        main.putExtra("category", selected);
        startActivity(main);
    }

    @Override
    public void onSheetDismissed(@NonNull BottomSheet bottomSheet, @Nullable Object o, int i) {

    }

    // endregion
}
