package com.melvinsalas.peppermint.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.kennyc.bottomsheet.BottomSheet;
import com.kennyc.bottomsheet.BottomSheetListener;
import com.melvinsalas.peppermint.AddWalletActivity;
import com.melvinsalas.peppermint.R;
import com.melvinsalas.peppermint.adapters.WalletsAdapter;
import com.melvinsalas.peppermint.database.Database;
import com.melvinsalas.peppermint.models.WalletModel;

import java.util.ArrayList;

public class WalletsFragment extends Fragment implements View.OnClickListener, BottomSheetListener {

    private View view;
    private ArrayList<WalletModel> wallets;
    private WalletModel selected;

    public WalletsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSheetShown(@NonNull BottomSheet bottomSheet, @Nullable Object o) {

    }

    @Override
    public void onSheetItemSelected(@NonNull BottomSheet bottomSheet, MenuItem menuItem, @Nullable Object o) {
        switch (menuItem.getItemId()) {
            case R.id.edit:
                editWallet();
                break;
            case R.id.delete:
                removeWallet(selected);
                break;
        }
    }

    @Override
    public void onSheetDismissed(@NonNull BottomSheet bottomSheet, @Nullable Object o, int i) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_wallets, container, false);
        addListener();
        wallets = new ArrayList<>();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addFab:
                addWallet();
                break;
        }
    }

    private void addListener() {
        getAddFab().setOnClickListener(this);
        addChildEventListener();
        getWallets().setOnItemLongClickListener((parent, view, position, id) -> {
            selected = wallets.get(position);
            openMenu();
            return false;
        });
    }

    private void openMenu() {
        new BottomSheet.Builder(getActivity())
                .setSheet(R.menu.item)
                .setTitle("Title")
                .setListener(this)
                .show();
    }

    private void addWallet() {
        Intent main = new Intent(getActivity(), AddWalletActivity.class);
        startActivity(main);
    }

    private void editWallet() {
        Intent main = new Intent(getActivity(), AddWalletActivity.class);
        main.putExtra("wallet", selected);
        startActivity(main);
    }

    private void removeWallet(WalletModel selected) {
        Database.getReference()
                .child("wallets").child(selected.getId()).removeValue();
    }

    private void addChildEventListener() {
        ChildEventListener childListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                WalletModel wallet = dataSnapshot.getValue(WalletModel.class);
                wallet.setId(dataSnapshot.getKey());
                walletAdded(wallet);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                WalletModel wallet = dataSnapshot.getValue(WalletModel.class);
                wallet.setId(dataSnapshot.getKey());
                walletChanged(wallet);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                WalletModel wallet = dataSnapshot.getValue(WalletModel.class);
                wallet.setId(dataSnapshot.getKey());
                walletRemoved(wallet);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Database.getReference()
                .child("wallets").orderByChild("owner").equalTo(uid)
                .addChildEventListener(childListener);
    }

    private void walletRemoved(final WalletModel wallet) {
        wallets.removeIf(w -> w.getId().equals(wallet.getId()));
        if(getActivity() != null) {
            WalletsAdapter adapter = new WalletsAdapter(getActivity(), wallets);
            getWallets().setAdapter(adapter);
        }
    }

    private void walletChanged(final WalletModel wallet) {
        for(int i = 0; i < wallets.size(); i++) {
            if(wallets.get(i).getId().equals(wallet.getId())) {
                wallets.set(i, wallet);
            }
        }
        if(getActivity() != null) {
            WalletsAdapter adapter = new WalletsAdapter(getActivity(), wallets);
            getWallets().setAdapter(adapter);
        }
    }

    private void walletAdded(WalletModel wallet) {
        wallets.add(wallet);
        if(getActivity() != null) {
            WalletsAdapter adapter = new WalletsAdapter(getActivity(), wallets);
            getWallets().setAdapter(adapter);
        }
    }

    // region Get & Set

    private FloatingActionButton getAddFab() {
        return view.findViewById(R.id.addFab);
    }

    private ListView getWallets() {
        return view.findViewById(R.id.walletList);
    }

    // endregion
}
