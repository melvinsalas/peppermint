package com.melvinsalas.peppermint.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.melvinsalas.peppermint.R;
import com.melvinsalas.peppermint.database.Database;
import com.melvinsalas.peppermint.models.WalletModel;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment {

    private View view;
    private ArrayList<WalletModel> wallets;
    private List<PieEntry> entries;
    private List<Integer> colors;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        addChildEventListener();
        wallets = new ArrayList<>();
        entries = new ArrayList<>();
        return view;
    }

    private void addChildEventListener() {
        ChildEventListener childListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                WalletModel wallet = dataSnapshot.getValue(WalletModel.class);
                wallet.setId(dataSnapshot.getKey());
                walletAdded(wallet);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                WalletModel wallet = dataSnapshot.getValue(WalletModel.class);
                wallet.setId(dataSnapshot.getKey());
                walletChanged(wallet);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                WalletModel wallet = dataSnapshot.getValue(WalletModel.class);
                wallet.setId(dataSnapshot.getKey());
                walletRemoved(wallet);
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Database.getReference()
                .child("wallets").orderByChild("owner").equalTo(uid)
                .addChildEventListener(childListener);
    }

    private void walletRemoved(final WalletModel wallet) {
        wallets.removeIf(w -> w.getId().equals(wallet.getId()));
        updateChart();
    }

    private void walletChanged(final WalletModel wallet) {
        for(int i = 0; i < wallets.size(); i++) {
            if(wallets.get(i).getId().equals(wallet.getId())) {
                wallets.set(i, wallet);
            }
        }
        updateChart();
    }

    private void walletAdded(WalletModel wallet) {
        wallets.add(wallet);
        updateChart();
    }

    private void updateChart() {
        entries = new ArrayList<>();
        colors = new ArrayList<>();
        for(WalletModel w: wallets) {
            entries.add(new PieEntry(w.getValue(), w.name));
            colors.add(w.color);
        }
        PieDataSet set = new PieDataSet(entries, "");
        set.setColors(colors);
        PieData data = new PieData(set);
        getChart().setData(data);
        getChart().invalidate();
    }

    // region Get & Set

    public PieChart getChart() {
        return view.findViewById(R.id.chart);
    }

    // endregion
}
