package com.melvinsalas.peppermint.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.melvinsalas.peppermint.AddTransactionActivity;
import com.melvinsalas.peppermint.R;
import com.melvinsalas.peppermint.adapters.TransactionsAdapter;
import com.melvinsalas.peppermint.database.Database;
import com.melvinsalas.peppermint.models.TransactionModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TransactionsFragment extends Fragment {

    @BindView(R.id.transactionList) ListView transactionList;

    private TransactionModel selected;
    private ArrayList<TransactionModel> transactions;

    public TransactionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_transactions, container, false);
        ButterKnife.bind(this, v);
        addListener();
        return v;
    }

    @OnClick(R.id.addFab)
    public void addTransaction() {
        startActivity(new Intent(getActivity(), AddTransactionActivity.class));
    }

    private void addListener() {
        addChildEventListener();
    }

    private void addChildEventListener() {
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                TransactionModel transaction = dataSnapshot.getValue(TransactionModel.class);
                transaction.setId(dataSnapshot.getKey());
                transactionsAdded(transaction);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Database.getReference()
                .child("transactions").orderByChild("owner").equalTo(uid)
                .addChildEventListener(childEventListener);
    }



    private void transactionsAdded(TransactionModel transaction) {
        if (transactions == null) {
            transactions = new ArrayList<>();
        }
        transactions.add(transaction);
        if (getActivity() != null) {
            TransactionsAdapter adapter = new TransactionsAdapter(getActivity(), transactions);
            transactionList.setAdapter(adapter);
        }
    }
}
