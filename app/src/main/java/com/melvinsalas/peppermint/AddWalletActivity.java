package com.melvinsalas.peppermint;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.color.ColorChooserDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.melvinsalas.peppermint.adapters.IconsAdapter;
import com.melvinsalas.peppermint.database.WalletManager;
import com.melvinsalas.peppermint.models.IconModel;
import com.melvinsalas.peppermint.models.WalletModel;
import com.melvinsalas.utils.TextInputLayoutUtil;

import java.util.ArrayList;

public class AddWalletActivity extends AppCompatActivity implements View.OnClickListener, ColorChooserDialog.ColorCallback {

    private int selectedColor;
    private WalletModel wallet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_wallet);
        addListener();

        Intent intent = getIntent();

        if(intent.hasExtra("wallet")){
            wallet = intent.getParcelableExtra("wallet");
            this.getName().getEditText().setText(wallet.name);
            this.getValue().getEditText().setText(wallet.value.toString());
            this.setSelectedColor(wallet.color);
            this.getCreateWallet().setText(R.string.save_changes);
        } else {
            wallet = null;
        }

        getChangeColorButton().setOnClickListener(v -> new ColorChooserDialog.Builder(AddWalletActivity.this, R.string.color_palette)
                .titleSub(R.string.add_wallet_title)
                .accentMode(false)
                .doneButton(R.string.md_done_label)
                .cancelButton(R.string.md_cancel_label)
                .backButton(R.string.md_back_label)
                .dynamicButtonColor(false)
                .show(AddWalletActivity.this));

        final ArrayList<IconModel> x = new ArrayList<>();
        x.add(new IconModel("Cash", "ic_cash"));
        x.add(new IconModel("Incognito", "ic_incognito"));
        x.add(new IconModel("Star", "ic_star"));
        final IconModel[] y = x.toArray(new IconModel[x.size()]);
        final IconsAdapter z = new IconsAdapter(this, y);

        getChangeIconButton().setOnClickListener(v -> new MaterialDialog.Builder(AddWalletActivity.this)
                .title("Select an icon")
                .adapter(z, null)
                .show());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.createWallet:
                if(wallet != null) {
                    updateWallet();
                } else {
                    createWallet();
                }
                break;
        }
    }

    @Override
    public void onColorSelection(@NonNull ColorChooserDialog dialog, int selectedColor) {
        setSelectedColor(selectedColor);
    }

    @Override
    public void onColorChooserDismissed(@NonNull ColorChooserDialog dialog) {

    }

    private void createWallet() {
        WalletManager.create(
                TextInputLayoutUtil.getString(getName()),
                TextInputLayoutUtil.getDouble(getValue()),
                getSelectedColor(),
                getOnCompleteListener());
    }

    private void updateWallet() {
        WalletManager.update(
                wallet,
                TextInputLayoutUtil.getString(getName()),
                TextInputLayoutUtil.getDouble(getValue()),
                getSelectedColor(),
                getOnCompleteListener());
    }

    private OnCompleteListener<Void> getOnCompleteListener() {
        return task -> {
            if (task.isSuccessful()) {
                finish();
            } else {
                Toast.makeText(AddWalletActivity.this, "Error.", Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void addListener() {
        getCreateWallet().setOnClickListener(this);
    }

    // region Get & Set

    private TextInputLayout getName() {
        return findViewById(R.id.nameInput);
    }

    private TextInputLayout getValue() {
        return findViewById(R.id.valueInput);
    }

    private Button getCreateWallet() {
        return findViewById(R.id.createWallet);
    }

    private View getIconBackground() {
        return findViewById(R.id.iconBackground);
    }

    private Button getChangeColorButton() {
        return findViewById(R.id.changeColorButton);
    }

    private Button getChangeIconButton() {
        return findViewById(R.id.changeIconButton);
    }

    public int getSelectedColor() {
        if(selectedColor == 0) {
            setSelectedColor(getResources().getColor(R.color.colorPrimary));
        }
        return selectedColor;
    }

    public void setSelectedColor(int selectedColor) {
        this.selectedColor = selectedColor;
        getIconBackground().setBackgroundColor(getSelectedColor());
    }

    // endregion
}
