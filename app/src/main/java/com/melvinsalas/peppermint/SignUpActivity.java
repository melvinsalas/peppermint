package com.melvinsalas.peppermint;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.melvinsalas.peppermint.models.SignUpUser;
import com.melvinsalas.utils.TextInputLayoutUtil;

import static com.melvinsalas.utils.TextInputLayoutUtil.validateEmail;
import static com.melvinsalas.utils.TextInputLayoutUtil.validatePassword;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        firebaseAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference();
        addListener();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUpButton:
                signUp();
                break;
        }
    }

    private void addListener() {
        getSignUp().setOnClickListener(this);
    }

    private void signUp() {
        if(isFormValid()) {
            if(validateRepeatPassword()) {
                String email = TextInputLayoutUtil.getString(getEmail());
                String password = TextInputLayoutUtil.getString(getPassword());

                if (email != null && password != null) {
                    firebaseAuth
                            .createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(getOnCompleteListener());
                }
            } else {
                Toast.makeText(
                        SignUpActivity.this,
                        "Passwords don't match.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean validateRepeatPassword() {
        return TextInputLayoutUtil.getString(getPassword())
                .equals(TextInputLayoutUtil.getString(getRepeatPassword()));
    }

    private void writeNewUser(String userId, String name, String email) {
        SignUpUser user = new SignUpUser(name, email);
        databaseReference.child("users").child(userId).setValue(user);
    }

    private OnCompleteListener<AuthResult> getOnCompleteListener() {
        return task -> {
            if (task.isSuccessful()) {
                writeNewUser(
                        firebaseAuth.getCurrentUser().getUid(),
                        TextInputLayoutUtil.getString(getEmail()),
                        TextInputLayoutUtil.getString(getName()));

                finish();
            } else {
                Toast.makeText(
                        SignUpActivity.this,
                        "Authentication failed.",
                        Toast.LENGTH_SHORT).show();
            }
        };
    }

    private boolean isFormValid() {
        return validateEmail(getEmail()) && validatePassword(getPassword());
    }

    // region Get & Set

    private TextInputLayout getName() {
        return findViewById(R.id.nameInput);
    }

    private TextInputLayout getEmail() {
        return findViewById(R.id.emailInput);
    }

    private TextInputLayout getPassword() {
        return findViewById(R.id.passwordInput);
    }

    private TextInputLayout getRepeatPassword() {
        return findViewById(R.id.repeatPasswordInput);
    }

    private Button getSignUp() {
        return findViewById(R.id.signUpButton);
    }

    // endregion
}
