package com.melvinsalas.peppermint;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.flags.impl.DataUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.melvinsalas.peppermint.database.CategoryManager;
import com.melvinsalas.peppermint.database.TransactionManager;
import com.melvinsalas.peppermint.database.WalletManager;
import com.melvinsalas.peppermint.models.CategoryModel;
import com.melvinsalas.peppermint.models.TransactionModel;
import com.melvinsalas.peppermint.models.WalletModel;
import com.melvinsalas.utils.TextInputLayoutUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddTransactionActivity extends AppCompatActivity implements OnCompleteListener<Void> {

    @BindView(R.id.toText) public TextView toText;
    @BindView(R.id.fromText) public TextView fromText;
    @BindView(R.id.categoryText) public TextView categoryText;
    @BindView(R.id.amountInput) public TextInputLayout amountInput;

    private ArrayList<WalletModel> wallets;
    private ArrayList<CategoryModel> categories;
    private WalletModel toWallet;
    private WalletModel fromWallet;
    private CategoryModel selectedCategory;

    /**
     * Init the activity view
     *
     * @param savedInstanceState The saved instance state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inflate View
        setContentView(R.layout.activity_add_transaction);
        // Bind View
        ButterKnife.bind(this);
        // Get Data
        WalletManager.getAll(loadWallets);
        CategoryManager.getAll(loadCategories);
    }

    @OnClick(R.id.createTransaction)
    public void createTransaction() {
        TransactionModel transaction = new TransactionModel(
                toWallet.getId(),
                fromWallet.getId(),
                TextInputLayoutUtil.getDouble(amountInput),
                selectedCategory.getId()
        );
        TransactionManager.create(transaction, this);
    }

    /**
     * To text on click listener
     */
    @OnClick(R.id.toText)
    public void selectTo() {
        ArrayList<String> list = new ArrayList<>();
        wallets.forEach(a -> list.add(a.getName()));
        new MaterialDialog.Builder(this)
                .title("Select a wallet")
                .items(list)
                .itemsCallbackSingleChoice(-1, (dialog, view, which, text) -> {
                    toWallet = wallets.get(which);
                    toText.setText(toWallet.getName());
                    return true;
                })
                .positiveText("Choose")
                .show();
    }

    /**
     * To text on click listener
     */
    @OnClick(R.id.fromText)
    public void selectFrom() {
        ArrayList<String> list = new ArrayList<>();
        wallets.forEach(a -> list.add(a.getName()));
        new MaterialDialog.Builder(this)
                .title("Select a wallet")
                .items(list)
                .itemsCallbackSingleChoice(-1, (dialog, view, which, text) -> {
                    fromWallet = wallets.get(which);
                    fromText.setText(fromWallet.getName());
                    return true;
                })
                .positiveText("Choose")
                .show();
    }

    /**
     * To text on click listener
     */
    @OnClick(R.id.categoryText)
    public void selectCategory() {
        ArrayList<String> list = new ArrayList<>();
        categories.forEach(a -> list.add(a.getName()));
        new MaterialDialog.Builder(this)
                .title("Select a wallet")
                .items(list)
                .itemsCallbackSingleChoice(-1, (dialog, view, which, text) -> {
                    selectedCategory = categories.get(which);
                    categoryText.setText(selectedCategory.getName());
                    return true;
                })
                .positiveText("Choose")
                .show();
    }

    /**
     * Load Wallets ValueEventListener
     */
    private ValueEventListener loadWallets = new ValueEventListener() {

        /**
         * Load the result data into application
         *
         * @param dataSnapshot The data snapshot
         */
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            wallets = new ArrayList<>();
            for (DataSnapshot child : dataSnapshot.getChildren()) {
                WalletModel item = child.getValue(WalletModel.class);
                if (item != null) {
                    item.setId(child.getKey());
                    wallets.add(item);
                }
            }
        }

        /**
         * Catch the database connection issues
         *
         * @param databaseError The database error
         */
        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            // TODO: Notify listener cancellation
        }
    };

    /**
     * Load Categories ValueEventListener
     */
    private ValueEventListener loadCategories = new ValueEventListener() {

        /**
         * Load the result data into application
         *
         * @param dataSnapshot The data snapshot
         */
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            categories = new ArrayList<>();
            for (DataSnapshot child : dataSnapshot.getChildren()) {
                CategoryModel item = child.getValue(CategoryModel.class);
                if (item != null) {
                    item.setId(child.getKey());
                    categories.add(item);
                }
            }
        }

        /**
         * Catch the database connection issues
         *
         * @param databaseError The database error
         */
        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            // TODO: Notify listener cancellation
        }
    };

    @Override
    public void onComplete(@NonNull Task<Void> task) {
        if (task.isSuccessful()) {
            finish();
        } else {
            Toast.makeText(AddTransactionActivity.this, "Error.", Toast.LENGTH_SHORT).show();
        }
    }
}
