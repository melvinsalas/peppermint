package com.melvinsalas.peppermint.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.melvinsalas.peppermint.R;
import com.melvinsalas.peppermint.models.IconModel;

public class IconsAdapter extends RecyclerView.Adapter<IconsAdapter.ViewHolder> {

    private IconModel[] mDataset;
    private Context context;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.icon_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        IconModel model = mDataset[position];
        ((TextView)holder.mRoot.findViewById(R.id.nameText)).setText(model.getName());
        int drawable = context.getResources().getIdentifier(model.getIcon(), "drawable", context.getPackageName());
        ((ImageView)holder.mRoot.findViewById(R.id.iconImage)).setImageDrawable(context.getDrawable(drawable));
    }

    @Override
    public int getItemCount() {
        return mDataset.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View mRoot;
        public ViewHolder(View v) {
            super(v);
            mRoot = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public IconsAdapter(Context context, IconModel[] myDataset) {
        mDataset = myDataset;
        this.context = context;
    }
}
