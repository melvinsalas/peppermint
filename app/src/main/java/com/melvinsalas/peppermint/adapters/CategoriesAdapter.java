package com.melvinsalas.peppermint.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.melvinsalas.peppermint.R;
import com.melvinsalas.peppermint.models.CategoryModel;

import java.util.ArrayList;

public class CategoriesAdapter extends ArrayAdapter<CategoryModel> {

    public CategoriesAdapter(Context context, ArrayList<CategoryModel> categories) {
        super(context, 0, categories);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CategoryModel categories = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.wallet_item, parent, false);
        }

        ImageView image =  convertView.findViewById(R.id.imageView);
        TextView name = convertView.findViewById(R.id.nameText);

        TextDrawable drawable = TextDrawable.builder()
                .buildRoundRect(categories.name.substring(0, 1), categories.color, 10);
        image.setImageDrawable(drawable);
        name.setText(categories.name);

        return convertView;
    }
}
