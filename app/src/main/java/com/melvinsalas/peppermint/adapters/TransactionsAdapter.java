package com.melvinsalas.peppermint.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.melvinsalas.peppermint.R;
import com.melvinsalas.peppermint.models.TransactionModel;

import java.util.ArrayList;

public class TransactionsAdapter extends ArrayAdapter<TransactionModel> {

    public TransactionsAdapter(@NonNull Context context, ArrayList<TransactionModel> transactions) {
        super(context, 0, transactions);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TransactionModel transaction = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.transaction_item, parent, false);
        }

        TextView amountText = convertView.findViewById(R.id.amountText);

        amountText.setText(transaction.amount + "");

        return convertView;
    }
}
