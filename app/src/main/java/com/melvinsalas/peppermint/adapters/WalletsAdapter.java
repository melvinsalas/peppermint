package com.melvinsalas.peppermint.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.melvinsalas.peppermint.R;
import com.melvinsalas.peppermint.models.WalletModel;

import java.util.ArrayList;

public class WalletsAdapter extends ArrayAdapter<WalletModel> {
    public WalletsAdapter(Context context, ArrayList<WalletModel> wallets) {
        super(context, 0, wallets);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        WalletModel wallet = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.wallet_item, parent, false);
        }

        ImageView image =  convertView.findViewById(R.id.imageView);
        TextView name = convertView.findViewById(R.id.nameText);
        TextView value = convertView.findViewById(R.id.valueText);

        TextDrawable drawable = TextDrawable.builder()
                .buildRoundRect(wallet.name.substring(0, 1), wallet.color, 10);
        image.setImageDrawable(drawable);
        name.setText(wallet.name);
        value.setText("¢ " + wallet.value.toString());

        return convertView;
    }
}
