package com.melvinsalas.peppermint.models;

public class TransactionModel {

    private String id;
    private String owner;
    public String to;
    public String from;
    public Double amount;
    public String category;

    public TransactionModel() {

    }

    public TransactionModel(String to, String from, Double amount, String category) {
        this.to = to;
        this.from = from;
        this.amount = amount;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
