package com.melvinsalas.peppermint.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class CategoryModel implements Parcelable {

    private String id;
    public String owner;
    public String name;
    public int color;

    public CategoryModel() {
        // Model need public constructor
    }

    /**
     * Constructor
     *
     * @param owner The category owner
     * @param name The category name
     * @param color The category color
     */
    public CategoryModel(String owner, String name, int color) {
        this.owner = owner;
        this.name = name;
        this.color = color;
    }

    protected CategoryModel(Parcel in) {
        id = in.readString();
        owner = in.readString();
        name = in.readString();
        color = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(owner);
        dest.writeString(name);
        dest.writeInt(color);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CategoryModel> CREATOR = new Creator<CategoryModel>() {
        @Override
        public CategoryModel createFromParcel(Parcel in) {
            return new CategoryModel(in);
        }

        @Override
        public CategoryModel[] newArray(int size) {
            return new CategoryModel[size];
        }
    };

    // region Get & Set

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    // endregion
}
