package com.melvinsalas.peppermint.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class WalletModel implements Parcelable {

    private String id;      // Wallet identifier
    public String name;     // Wallet name
    public Double value;    // Wallet amount
    public String owner;    // Wallet owner id
    public int color;       // Wallet color

    public WalletModel() {
        // Model need public constructor
    }

    /**
     * Constructor
     *
     * @param owner Owner database id
     * @param name Wallet name
     * @param value Wallet value
     * @param color Wallet color
     */
    public WalletModel(String owner, String name, Double value, int color) {
        this.name = name;
        this.value = value;
        this.owner = owner;
        this.color = color;
    }

    protected WalletModel(Parcel in) {
        id = in.readString();
        name = in.readString();
        if (in.readByte() == 0) {
            value = null;
        } else {
            value = in.readDouble();
        }
        owner = in.readString();
        color = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        if (value == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(value);
        }
        dest.writeString(owner);
        dest.writeInt(color);
    }

    public static final Creator<WalletModel> CREATOR = new Creator<WalletModel>() {
        @Override
        public WalletModel createFromParcel(Parcel in) {
            return new WalletModel(in);
        }

        @Override
        public WalletModel[] newArray(int size) {
            return new WalletModel[size];
        }
    };

    // region Get & Set

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getValue() {
        return (float)(double)value;
    }

    public String getName() {
        return name;
    }

    // endregion
}
