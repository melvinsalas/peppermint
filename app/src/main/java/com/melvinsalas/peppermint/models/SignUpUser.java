package com.melvinsalas.peppermint.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class SignUpUser {

    public String email;
    public String name;

    public SignUpUser() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public SignUpUser(String email, String name) {
        this.email = email;
        this.name = name;
    }

}
