package com.melvinsalas.peppermint;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.melvinsalas.peppermint.fragments.AccountFragment;
import com.melvinsalas.peppermint.fragments.CategoriesFragment;
import com.melvinsalas.peppermint.fragments.DashboardFragment;
import com.melvinsalas.peppermint.fragments.TransactionsFragment;
import com.melvinsalas.peppermint.fragments.WalletsFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.navigation) BottomNavigationViewEx navigation;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
        = item -> {
            Fragment fragment = null;
            String title = getString(R.string.app_name);

            switch (item.getItemId()) {
                case R.id.navigation_dashboard:
                    fragment = new DashboardFragment();
                    title = getResources().getString(R.string.title_dashboard);
                    break;
                case R.id.navigation_wallets:
                    fragment = new WalletsFragment();
                    title = getResources().getString(R.string.title_wallets);
                    break;
                case R.id.navigation_transactions:
                    fragment = new TransactionsFragment();
                    title = getResources().getString(R.string.title_transactions);
                    break;
                case R.id.navigation_categories:
                    fragment = new CategoriesFragment();
                    title = getResources().getString(R.string.title_categories);
                    break;
                case R.id.navigation_account:
                    fragment = new AccountFragment();
                    title = getResources().getString(R.string.title_account);
                    break;
            }

            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.contentFrame, fragment);
                ft.commit();

                if (getSupportActionBar() != null) {
                    setTitle(title);
                }

                return true;
            }

            return false;
        };

    public FrameLayout getContentFrame() {
        return findViewById(R.id.contentFrame);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_dashboard);
        navigation.enableAnimation(false);
        navigation.enableShiftingMode(false);
        navigation.enableItemShiftingMode(false);
    }

    private void setTitle(String title) {
        TextView titleText =  getSupportActionBar().getCustomView().findViewById(R.id.title);
        titleText.setText(title);
    }

}
