package com.melvinsalas.utils;

import android.support.design.widget.TextInputLayout;
import android.util.Patterns;
import android.widget.EditText;

public class TextInputLayoutUtil {

    public static boolean validateEmail(TextInputLayout inputLayout) {
        // Retrieve data
        String email = getString(inputLayout);
        if(email == null) {
            return false;
        }
        boolean matches = Patterns.EMAIL_ADDRESS.matcher(email).matches();

        // Return matches
        if(matches) {
            inputLayout.setErrorEnabled(false);
            return true;
        } else {
            inputLayout.setError("Invalid email format");
            return false;
        }
    }

    public static boolean validatePassword(TextInputLayout inputLayout) {
        // Retrieve data
        String password = getString(inputLayout);
        if(password == null) {
            return false;
        }

        // Return matches
        if(password.length() > 5) {
            inputLayout.setErrorEnabled(false);
            return true;
        } else {
            inputLayout.setError("Password too short");
            return false;
        }
    }

    public static String getString(TextInputLayout inputLayout) {
        // Validate nullable params
        EditText editText = inputLayout.getEditText();
        if(editText == null) {
            return null;
        }

        // Retrieve data
        return editText.getText().toString();
    }

    public static double getDouble(TextInputLayout inputLayout) {
        String text = getString(inputLayout);
        if(text == null) return 0.0;
        return Double.parseDouble(getString(inputLayout));
    }
}
